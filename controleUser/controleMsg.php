<?php

session_start();

function setMsgErr($msg) {
        $_SESSION["msgErro"]= $msg;
   }

function getMsgErr() {
    
  if(isset($_SESSION["msgErro"]) && $_SESSION["msgErro"] != "") {
        $tmp =  $_SESSION["msgErro"];
        $_SESSION["msgErro"]= "";
        return $tmp;
    }else{
        return "";
    }
}

function setMsgOk($msg) {
        $_SESSION["msgOk"]= $msg;
    }

function getMsgOk() {
    
  if(isset($_SESSION["msgOk"])) {
        $tmp =  $_SESSION["msgOk"];
        $_SESSION["msgOk"]= "";
        return $tmp;
        }else{
        return "";
    }
}