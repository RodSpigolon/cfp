<?php
require_once("controleMsg.php");


function setUsuarioLogado($usuario,$nome,$adm,$id) {
    $_SESSION["user"]= $usuario;
    $_SESSION["nome"]= $nome;
    $_SESSION["adm"]= $adm;
    $_SESSION["id"]= $id;
}
function logout() {
     session_destroy();
}
function getUsuarioLogado() {
    
  return $_SESSION['user'];
}
function getIdUsuarioLogado() {
    
  return $_SESSION['id'];
}
function getAdmLogado() {
    
  return $_SESSION['adm'];
}
function getNomeLogado() {
    
  return $_SESSION['nome'];
}
function existeUsuarioAut() {
    if(isset($_SESSION["user"]) && $_SESSION["user"] != "") {
        return true;
    }else{
        return false;
    }
}

function protege(){
   
if(existeUsuarioAut()==false) {
    setMsgErr("Faça o Login");
    //header("location: telaLogin.php");
    
}else{
 if(isset($_SESSION["adm"]) && strcasecmp($_SESSION["adm"],"S")==0 ){
     setMsgOk("Bem Vindo Administrador");
   }else{
    setMsgErr("Área Restrita ao Administrador");
    header("location: telaLogin.php"); 
   }
}

}
function protege2(){
   
if(existeUsuarioAut()==false) {
    setMsgErr("Faça o Login");
    header("location: telaLogin.php");
    
}
}

