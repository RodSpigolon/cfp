<h1>
    <?php echo "Listagem de Contas"; ?>
</h1>
<script type="text/javascript" src="js/bootstrap-filestyle.min.js"></script>
<?php
protege2();
$ano = $_GET['ano'];
$mes = $_GET['mes'];

if (isset($_GET["pagAtual"])) {
    $pagAtual = $_GET["pagAtual"];
} else {
    $pagAtual = 1;
}

@$ls = listar($conexao, $ano, $mes, $pagAtual, 6, getIdUsuarioLogado());
?>

<p class="alert-danger">
    <?php echo getMsgErr(); ?>
</p>
<p class="alert-success">
    <?php echo getMsgOk(); ?>
</p>
<table class="table table-bordered table-striped">
    <th> Data Vencimento </th>
    <th> Descrição </th>
    <th> Valor </th>
    <th> Operação </th>
    <th> Situação </th>
    <?php
    foreach ($ls as $conta) {
        if ($conta['dataVencimento'] != "") {
            ?>
            <tr>
                <?php
                $data = implode('/', array_reverse(explode('-', $conta['dataVencimento'])));
                ?>
                <td> <?php echo($data); ?>  </td>
                <td> <?php echo($conta["descricao"]); ?> </td>
                <td> <?php echo("R$" . number_format($conta["valor"], 2, ',', '.')); ?> </td> 
                <td> <?= $conta["operacao"] == "E" ? "Entrada" : "Saida"; ?> </td>
                <td> <?= $conta["situacao"] == "A" ? "Aberta" : "Fechada"; ?> </td>
                <td> <a class="btn btn-primary" href="index.php?pg=altConta&id=<?php echo($conta["id"]); ?>"> Alterar </a> </td>  
                <td> 
                    <form action="controleContas/removeConta.php" method="post">
                        <input type="hidden" name="id" value="<?php echo($conta["id"]); ?>"/>
                        <input class="btn btn-danger" type="submit" value="Remover" />
                    </form>   
                </td>   
            </tr>
            <?php
        }
    }
    ?>
</table>

<?php
$qtdTotalReg = qtdContas($conexao, $mes, $ano,  intval(getIdUsuarioLogado()));
$qntTotalPag = $qtdTotalReg["count(*)"] / 6;
if (intval($qntTotalPag) == 0) {
    $qntTotalPag = 1;
} else {
    $qntTotalPag = intval($qntTotalPag);
}
if ($pagAtual == $qntTotalPag) {
    $idS = getIdUsuarioLogado();
    $idS = (intval($idS));
    $somaE = somaContasE($conexao, $mes, $ano, $idS);
    $somaS = somaContasS($conexao, $mes, $ano, $idS);
    ?> <p class="alert-success">
    <?php echo "Total: R$" . number_format(($somaE["sum(valor)"] - $somaS["sum(valor)"]),2, ',', '.'); ?>
    </p>
    <?php
}
for ($i = 1; $i <= intval($qntTotalPag); $i++) {
    ?>
    <div id="pag" class="pagination">
        <ul>
            <li><a class="btn btn-primary" href="index.php?pg=pagination&mes=<?= $mes; ?>&ano=<?= $ano; ?>&pagAtual=<?= $i; ?>"> <?= $i; ?></a></li>
        </ul>
    </div>
    <?php
}
?>
<br>
<div class="input-group" id="import">
    <a class="btn btn-primary" href="controleFPDF/trataAqv.php?id=2&mes=<?= $mes; ?>&ano=<?= $ano; ?>&idUser=<?= getIdUsuarioLogado(); ?>"> Gerar Relatorio PDF </a> 
    <a class="btn btn-primary" href="controleFPDF/trataAqv.php?id=1&mes=<?= $mes; ?>&ano=<?= $ano; ?>&idUser=<?= getIdUsuarioLogado(); ?>"> Exportar Produtos </a> 
    <br>
    <br>
<!--
    <div id="filee">
        <form name="impArq" action="controleFPDF/trataAqv.php" method="POST" onsubmit="return valida()" enctype="multipart/form-data">
            <input type="hidden" name="idImp" value="2">
            <input class="filestyle" data-badge="false"data-buttonName="btn-primary" type="file" name="arquivo">
            <br>

            <input class="btn btn-primary" type="submit" value="Importar Produtos" />
        </form>
    </div> -->
</div>