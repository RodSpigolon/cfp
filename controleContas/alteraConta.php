<?php
protege2();
$id = $_GET["id"];
$conta = buscaConta($conexao, $id);
?>
<script>
$(function() {
    $( "#calendario" ).datepicker({dateFormat: 'yy-mm-dd'});
});
</script>
<h1>Alterar Conta</h1>
<p class="alert-danger">
    <?php echo getMsgErr(); ?>
</p>
<p class="alert-success">
    <?php echo getMsgOk(); ?>
</p>

<form class="form-group" action="controleContas/trataAlteraConta.php?id=<?php echo($id); ?>" method="post">

    <p>Data de Vencimento: <br><input type="text" id="calendario" name="dataValidade" value="<?=$conta["dataVencimento"];?>"/></p>
            <br>
            Descrição: <input class="form-control" type="text" name="descricao" value="<?=$conta["descricao"];?>"/>
            <br>
            Valor: <input class="form-control" type="text" name="valor" value="<?=$conta["valor"];?>"/>
            <br>
              Operação: <select name="operacao">
              <option value=""> Escolha</option>
              <option  <?php echo ($conta["operacao"] == "E") ? "selected" : "";?> value="E"> Entrada</option>
              <option  <?php echo ($conta["operacao"] == "S") ? "selected" : "";?> value="S"> Saida</option>
              </select>
            <br> 
            <br> 
         
              Situação: <select name="situacao">
              <option value=""> Escolha</option>
              <option <?php echo ($conta["situacao"] == "A") ? "selected" : "";?> value="A"> Em Aberto</option>
              <option <?php echo ($conta["situacao"] == "F") ? "selected" : "";?> value="F"> Fechada</option>
              </select>
            <br> 
            <br> 
            <br> 
            
            <input class="btn-primary" type="submit" value="Salvar"/>
        </form>