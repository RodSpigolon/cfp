<?php
$ls = listaAno($conexao);
protege2();
?>
<div class="alert-success">
<?php  echo getMsgOk(); ?>
</div>
<div class="alert-danger">
<?php  echo getMsgErr(); ?>
</div>
<h1>Listar Contas</h1>
<?php
$anoatual = date ("Y");
$mesatual = date ("m");
?> 

<form class="form-group" action="index.php" method="GET">
    <input type="hidden" name="pg" value="listaconta" />
    
    Selecione o Mês: <select name="mes" >
        <option <?php echo ($mesatual == 01) ? "selected" : "";?> value="1"> Janeiro</option>
        <option <?php echo ($mesatual == 02) ? "selected" : "";?> value="2"> Fevereiro</option>
        <option <?php echo ($mesatual == 03) ? "selected" : "";?> value="3"> Março </option>
        <option <?php echo ($mesatual == 04) ? "selected" : "";?> value="4"> Abril</option>
        <option <?php echo ($mesatual == 05) ? "selected" : "";?> value="5"> Maio</option>
        <option <?php echo ($mesatual == 06) ? "selected" : "";?> value="6"> Junho</option>
        <option <?php echo ($mesatual == 07) ? "selected" : "";?> value="7"> Julho</option>
        <option <?php echo ($mesatual == 08) ? "selected" : "";?> value="8"> Agosto</option>
        <option <?php echo ($mesatual == 09) ? "selected" : "";?> value="9"> Setembro</option>
        <option <?php echo ($mesatual == 10) ? "selected" : "";?> value="10"> Outubro</option>
        <option <?php echo ($mesatual == 11) ? "selected" : "";?> value="11"> Novembro</option>
        <option <?php echo ($mesatual == 12) ? "selected" : "";?> value="12"> Dezembro</option>
    </select>
    <br> 
    <br> 
    <br> 
    Selecione o Ano: <select name="ano">  
           <?php
        foreach ($ls as $ano) {
            ?>
            <option <?php echo ($anoatual == $ano['Year(dataVencimento)']) ? "selected" : "";?> value="<?= $ano['Year(dataVencimento)'] ?>"> <?= $ano['Year(dataVencimento)']; ?> </option>

            <?php
        }
        ?>
    </select>
    
    <br>
    <br>
    <input class="btn-primary" type="submit" value="Pesquisar"/>
</form>


    <?php require_once("rodape.php"); ?>
