<?php

require_once("./cabecalho.php");

if (isset($_GET[sair])) {
    logout();
    header("location:index.php");
} else {
    $usuario = $_POST["user"];
    $senha = md5($_POST["password"]);

    $usuarios = buscaUsuario($conexao, $usuario, $senha);

    if ($usuarios["user"] != "") {
        setUsuarioLogado($usuarios["user"], $usuarios["nome"], $usuarios["admin"],$usuarios["id"]);
        header("location:index.php");
    } else {
        setMsgErr("Usuário e/ou Senha Invalidos");
        header("location:telaLogin.php");
    }
}



