<?php require_once("cabecalho.php"); ?>


<?php
if (isset($_GET['pg'])) {
    $pg = $_GET['pg'];
    if ($pg == 'addconta') {
        include "controleContas/adicionaConta.php";
    }
    elseif ($pg == 'selectconta') {
        include "controleContas/selectListaConta.php";
    }
    elseif ($pg == 'adicionaUser') {
        include "./controleUser/adicionaUser.php";
    }
    elseif ($pg == 'telaLogin') {
        include "./telaLogin.php";
    }
    elseif ($pg == 'listaconta') {
        include "controleContas/listaConta.php";
    }
    elseif ($pg == 'taddconta') {
        include "controleContas/trataAdicionaConta.php";
    }
    elseif ($pg == 'pagination') {//pg=pagination;pagAtual
        include "controleContas/listaConta.php";
    }
    elseif ($pg == 'trataAddUser') {//pg=pagination;pagAtual
        include "./controleUser/trataAddUser.php";
    }
    elseif ($pg == 'trataArq') {//pg=pagination;pagAtual
        include "./controleFPDF/trataAqv.php";
    }
    elseif ($pg == 'altConta') {//pg=pagination;pagAtual
        include "./controleContas/alteraConta.php";
    }
}else{?>
<div class="alert-success">
<?php  echo getMsgOk(); ?>
</div>
<div class="alert-danger">
<?php  echo getMsgErr(); ?>
</div>

   <div id="index">
   <h2>
    <?php echo "Controle Financeiro Pessoal"; ?>
</h2> 
</div>

<?php
}
?>

<?php
require_once("rodape.php");
