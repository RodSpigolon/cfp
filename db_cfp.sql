-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 14-Jun-2017 às 23:56
-- Versão do servidor: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_cfp`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `conta`
--

CREATE TABLE IF NOT EXISTS `conta` (
`id` int(11) NOT NULL,
  `dataVencimento` date NOT NULL,
  `descricao` varchar(500) NOT NULL,
  `valor` double NOT NULL,
  `operacao` varchar(1) NOT NULL,
  `situacao` varchar(1) NOT NULL,
  `user` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=169 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `conta`
--

INSERT INTO `conta` (`id`, `dataVencimento`, `descricao`, `valor`, `operacao`, `situacao`, `user`) VALUES
(5, '2017-02-09', 'Teste 04', 235, 'E', 'A', 2),
(8, '2016-03-17', 'Teste 06', 280, 'S', 'F', 2),
(9, '2016-03-22', 'Teste 07', 190, 'S', 'F', 2),
(10, '2016-03-25', 'Teste 08', 1200, 'E', 'F', 2),
(11, '2016-03-29', 'Teste 09', 622.6, 'S', 'A', 2),
(12, '2016-03-06', 'Teste 10', 987, 'S', 'F', 2),
(13, '2016-03-03', 'Teste 11', 741, 'S', 'F', 2),
(14, '2016-03-29', 'Teste 12', 590, 'E', 'F', 2),
(15, '2016-03-15', 'Teste 13', 877, 'E', 'F', 2),
(16, '2016-03-21', 'Teste 15', 222, 'E', 'A', 2),
(17, '2016-03-04', 'Teste 14', 10, 'S', 'F', 2),
(18, '2016-03-29', 'Teste 16', 22, 'E', 'A', 2),
(19, '2016-03-24', 'Teste 13', 1200, 'E', 'A', 2),
(23, '2016-03-16', 'Teste 01', 190, 'E', 'A', 2),
(29, '2016-03-24', 'Teste 13', 1200, 'E', 'A', 2),
(35, '2016-03-23', 'Teste 02', 250, 'E', 'F', 2),
(41, '2016-03-29', 'Teste 09', 622.6, 'S', 'A', 2),
(47, '2016-03-16', 'Teste 01', 190, 'E', 'A', 2),
(53, '2016-03-24', 'Teste 13', 1200, 'E', 'A', 2),
(59, '2016-03-23', 'Teste 02', 250, 'E', 'F', 2),
(65, '2016-03-29', 'Teste 09', 622.6, 'S', 'A', 2),
(71, '2016-03-16', 'Teste 01', 190, 'E', 'A', 2),
(77, '2016-03-24', 'Teste 13', 1200, 'E', 'A', 2),
(83, '2016-03-23', 'Teste 02', 250, 'E', 'F', 2),
(89, '2016-03-29', 'Teste 09', 622.6, 'S', 'A', 2),
(95, '2016-03-16', 'Teste 01', 190, 'E', 'A', 2),
(96, '2016-03-29', 'Teste 12', 590, 'E', 'F', 2),
(97, '2016-03-21', 'Teste 15', 222, 'E', 'A', 2),
(98, '2016-03-29', 'Teste 16', 22, 'E', 'A', 2),
(99, '2016-03-25', 'Teste 08', 1200, 'E', 'F', 2),
(100, '2016-03-24', 'Teste 13', 1200, 'E', 'A', 2),
(102, '2016-03-09', 'Teste 05', 123, 'E', 'A', 2),
(103, '2017-02-09', 'Teste 04', 235, 'E', 'A', 2),
(105, '2016-03-23', 'Teste 02', 250, 'E', 'F', 2),
(106, '2016-03-15', 'Teste 13', 877, 'E', 'F', 2),
(107, '2016-03-04', 'Teste 14', 10, 'S', 'F', 2),
(108, '2016-03-03', 'Teste 11', 741, 'S', 'F', 2),
(109, '2016-03-06', 'Teste 10', 987, 'S', 'F', 2),
(110, '2016-03-29', 'Teste 09', 622.6, 'S', 'A', 2),
(111, '2016-03-22', 'Teste 07', 190, 'S', 'F', 2),
(112, '2016-03-17', 'Teste 06', 280, 'S', 'F', 2),
(115, '2016-03-16', 'Teste 01', 190, 'E', 'A', 2),
(116, '2016-03-29', 'Teste 12', 590, 'E', 'F', 2),
(117, '2016-03-21', 'Teste 15', 222, 'E', 'A', 2),
(118, '2016-03-29', 'Teste 16', 22, 'E', 'A', 2),
(119, '2016-03-25', 'Teste 08', 1200, 'E', 'F', 2),
(120, '2016-03-24', 'Teste 13', 1200, 'E', 'A', 2),
(122, '2016-03-09', 'Teste 05', 123, 'E', 'A', 2),
(123, '2017-02-09', 'Teste 04', 235, 'E', 'A', 2),
(125, '2016-03-23', 'Teste 02', 250, 'E', 'F', 2),
(126, '2016-03-15', 'Teste 13', 877, 'E', 'F', 2),
(127, '2016-03-04', 'Teste 14', 10, 'S', 'F', 2),
(128, '2016-03-03', 'Teste 11', 741, 'S', 'F', 2),
(129, '2016-03-06', 'Teste 10', 987, 'S', 'F', 2),
(130, '2016-03-29', 'Teste 09', 622.6, 'S', 'A', 2),
(131, '2016-03-22', 'Teste 07', 190, 'S', 'F', 2),
(132, '2016-03-17', 'Teste 06', 280, 'S', 'F', 2),
(133, '2015-12-17', 'Teste 03', 532, 'S', 'F', 2),
(154, '2016-01-19', 'Aluguel', 650, 'S', 'A', 3),
(155, '2016-01-25', 'Fatura do CartÃ£o', 367.28, 'S', 'F', 3),
(156, '2016-01-14', 'Salario', 1300, 'E', 'F', 3),
(159, '2015-01-01', 'Testee', 855, 'E', 'A', 4),
(160, '2015-01-21', 'Teste', 745.5, 'E', 'F', 2),
(161, '2017-06-22', 'Teste 2017', 150.5, 'S', 'A', 2),
(162, '2017-06-14', 'salario', 2500, 'E', 'A', 2),
(163, '2017-06-12', 'prsente paloma', 450, 'S', 'F', 2),
(164, '2017-06-09', 'roupas', 200, 'S', 'F', 2),
(165, '2017-06-02', 'casaco', 135.8, 'S', 'F', 2),
(166, '2017-06-14', 'AlmoÃ§o RU', 10.5, 'S', 'F', 2),
(167, '2017-06-15', 'Corte cabelo', 30, 'S', 'A', 2),
(168, '2017-06-21', 'Carona', 40, 'S', 'A', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
`id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `user` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `admin` varchar(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `nome`, `user`, `password`, `admin`) VALUES
(2, 'Administrador', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'S'),
(3, 'Rodrigo', 'rod', '52c59993d8e149a1d70b65cb08abf692', 'N'),
(4, 'Gilson', 'gilson', 'bc9255396302b613227a0f73fcf6b776', 'N');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `conta`
--
ALTER TABLE `conta`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `conta`
--
ALTER TABLE `conta`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=169;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
