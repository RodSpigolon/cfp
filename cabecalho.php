<?php
require_once("controleUser/controleMsg.php");
require_once("controleUser/controleUsuario.php");
require_once("controleDataBase/conecta.php");
require_once("controleDataBase/bdlogin.php");
require_once("controleDataBase/bdConta.php");
require_once("controleContas/conta.php");
?> 

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />
        <script src="http://code.jquery.com/jquery-1.8.2.js"></script>
        <script src="http://code.jquery.com/ui/1.9.0/jquery-ui.js"></script>
        <link href="css/bootstrap.css" rel="stylesheet"/>
        <link href="css/formatacao.css" rel="stylesheet"/>
        <script type="text/javascript" src="js/bootstrap-filestyle.min.js"> </script>
        <script type="text/javascript" src="js/funcoes.js"> </script>
    </head>
    <body>

        <div class="navbar navbar-inverse">    
            <ul id="menubventrar" >
                <div id="nav_bar">
                    <li><a href="index.php">Início</a></li>
                    <li><a href="?pg=addconta">Adicionar Nova Conta </a> </li>
                    <li><a href="?pg=selectconta">Listar Contas </a> </li>
                    <?php if (existeUsuarioAut() == true && strcasecmp(getAdmLogado(), "S") == 0) {
                        ?> <li><a href="?pg=adicionaUser">Cadastro de Usuários </a> </li> 
                            <?php
                        }
                        ?>

                    <?php
                    if (existeUsuarioAut() == true) {
                        $user = getNomeLogado();
                        ?>
                        <li>  <?php echo "<font color='#00BFFF'> $user </font>" ?> </li>
                        <li> <a href="trataLogin.php?sair">Sair</a> </li>
                    <?php } else {
                        ?> <li><a href="?pg=telaLogin">Login </a> </li>
                        <?php } ?>
                </div>
            </ul>
        </div>


