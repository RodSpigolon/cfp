
<?php

function consultaUsuarios($conexao) {
        $usuario = array(); 
        $sql = "select user, password, admin from usuario";
        $listaUsuario = mysqli_query($conexao, $sql); 
        $user = mysqli_fetch_assoc($listaUsuario); 
        while ($user != null) { 
            array_push($usuario, $user); 
            $user = mysqli_fetch_assoc($listaUsuario);        
        }
        return $usuario; 
    }
   
function buscaUsuario($conexao, $user, $password) {
    $prod = array();
    $sql = "select * from usuario where user = ? and password = ?";
    $sqlpreparado = $conexao->prepare($sql);
    $sqlpreparado->bind_param("ss", $user, $password);
    $sqlpreparado->execute();
    $sqlpreparado->bind_result(
            $prod["id"], 
            $prod["nome"], 
            $prod["user"], 
            $prod["password"],
            $prod["admin"]
            
    );
    $sqlpreparado->fetch();
    return $prod;
}