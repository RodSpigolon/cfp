<?php

function insereConta($conexao, $conta) {
    //public $dataValidade, $descricao, $valor, $operacao, $situacao;
    $sql = "insert into conta(dataVencimento,descricao,valor,operacao,situacao,user) values(?, ?, ?, ?, ?,?)"; //string sql que vai executada
    $sqlpreparado = $conexao->prepare($sql); //preparação do sql
    $sqlpreparado->bind_param('ssdssi', $conta->dataValidade, $conta->descricao, $conta->valor, $conta->operacao, $conta->situacao,$conta->id); //atribuindo valores para os parâmetros do SQL
    $sqlpreparado->execute(); //executa a instrução SQL
}
function insereUser($conexao, $nome, $user, $password,$adm) {
    $sql = "insert into usuario(nome,user,password,admin) values(?, ?, ?, ?)"; //string sql que vai executada
    $sqlpreparado = $conexao->prepare($sql); //preparação do sql
    $sqlpreparado->bind_param('ssss',$nome, $user, $password,$adm); //atribuindo valores para os parâmetros do SQL
    $sqlpreparado->execute(); //executa a instrução SQL
}

function alteraConta($conexao, $conta,$id) {
    $sql = "UPDATE conta SET dataVencimento = ?,descricao = ?,valor = ?,operacao = ?,situacao = ? WHERE id = ?"; //string sql que vai executada
    $sqlpreparado = $conexao->prepare($sql); //preparação do sql
    $sqlpreparado->bind_param('ssdssi', $conta->dataValidade, $conta->descricao, $conta->valor, $conta->operacao, $conta->situacao,$id); //atribuindo valores para os parâmetros do SQL
    $sqlpreparado->execute(); //executa a instrução SQL
}

function listaAno($conexao) {
    $vetorListaAno = array(); //cria o vetor/matriz
    $sql = "Select Year(dataVencimento) from conta group by Year(dataVencimento) "; //buscar os produtos da produtos
    $listaAnos = mysqli_query($conexao, $sql); //resultado do select é salvo em listaProdutos
    $ano = mysqli_fetch_assoc($listaAnos); //pegar o primeiro registro da listaProdutos
    while ($ano != null) { //repete se $produto não for nulo
        array_push($vetorListaAno, $ano); // adiciona o $produto na matriz $vetorListaProd
        $ano = mysqli_fetch_assoc($listaAnos);         //pegar o próximo registro
    }
    return $vetorListaAno; //retorna a matriz de produtos
}

function listaContasToExport($conexao) {
    $vetorListaConta = array(); //cria o vetor/matriz
    $sql = "SELECT * FROM  conta order by operacao"; //buscar os produtos da produtos
    $listaContas = mysqli_query($conexao, $sql); //resultado do select é salvo em listaProdutos
    $conta = mysqli_fetch_assoc($listaContas); //pegar o primeiro registro da listaProdutos
    while ($conta != null) { //repete se $produto não for nulo
        array_push($vetorListaConta, $conta); // adiciona o $produto na matriz $vetorListaProd
        $conta = mysqli_fetch_assoc($listaContas);         //pegar o próximo registro
    }
    return $vetorListaConta; //retorna a matriz de produtos
}

function qtdContas($conexao,$mes,$ano,$id) {
    $conta = array();
    $sql = "select count(*) from conta where Year(dataVencimento) = ? and Month(dataVencimento) = ? and id=?";
    $sqlpreparado = $conexao->prepare($sql);
    $sqlpreparado->bind_param("ssi", $ano,$mes,$id);
    $sqlpreparado->execute();
    $sqlpreparado->bind_result(
            $conta["count(*)"] 
                );
    $sqlpreparado->fetch();
    return $conta;
}
function somaContasE($conexao,$mes,$ano,$id) {
    $soma = array();
    $sql = "select sum(valor) from conta where Year(dataVencimento) = ? and Month(dataVencimento) = ? and operacao = 'E' and user = ?";
    $sqlpreparado = $conexao->prepare($sql);
    $sqlpreparado->bind_param("ssi", $ano,$mes,$id);
    $sqlpreparado->execute();
    $sqlpreparado->bind_result(
            $soma["sum(valor)"] 
                );
    $sqlpreparado->fetch();
    return $soma;
}

function somaContasS($conexao,$mes,$ano,$id) {
    $soma = array();
    $sql = "select sum(valor) from conta where Year(dataVencimento) = ? and Month(dataVencimento) = ? and operacao = 'S' and user = ?";
    $sqlpreparado = $conexao->prepare($sql);
    $sqlpreparado->bind_param("ssi", $ano,$mes,$id);
    $sqlpreparado->execute();
    $sqlpreparado->bind_result(
            $soma["sum(valor)"]
                );
    $sqlpreparado->fetch();
    return $soma;
}

function removeConta($conexao, $id) {
    $sql = "delete from conta where id=?";
    $sqlpreparado = $conexao->prepare($sql); //preparação do sql
    $sqlpreparado->bind_param('i', $id); //atribuindo valores para os parâmetros do SQL
    $sqlpreparado->execute(); //executa a instrução SQL        
}

  function buscaConta($conexao, $id) {
    $conta = array();
    $sql = "select * from conta where id=?";
    $sqlpreparado = $conexao->prepare($sql);
    $sqlpreparado->bind_param("i", $id);
    $sqlpreparado->execute();
    $sqlpreparado->bind_result(
            $conta["id"], 
            $conta["dataVencimento"], 
            $conta["descricao"],
            $conta["valor"],
            $conta["operacao"],
            $conta["situacao"],
            $conta["user"]  );
    $sqlpreparado->fetch();
    return $conta;
}

function listar($conexao,$ano,$mes,$pagAtual,$maxRegPag,$id) {
    $conta = array(); //cria o vetor/matriz
    $vetorListaConta = array();
    $inicio = ($pagAtual*$maxRegPag)-$maxRegPag;
    $sql = "SELECT  *  FROM  conta where Year(dataVencimento) = ? and Month(dataVencimento) = ? and user = ? order by operacao LIMIT $inicio,$maxRegPag"; 
    $sqlpreparado = $conexao->prepare($sql);
    $sqlpreparado->bind_param("ssi", $ano,$mes,$id);
    $sqlpreparado->execute();
    do{
    $sqlpreparado->bind_result(
            $conta["id"], 
            $conta["dataVencimento"], 
            $conta["descricao"],
            $conta["valor"],
            $conta["operacao"],
            $conta["situacao"],
            $conta["user"]  );
    array_push($vetorListaConta, $conta); 
    $conta = null;
    }while($sqlpreparado->fetch());
    return $vetorListaConta;
}
function listarToPDF($conexao,$ano,$mes,$id) {
    $conta = array(); //cria o vetor/matriz
    $vetorListaConta = array();
    $sql = "SELECT  *  FROM  conta where Year(dataVencimento) = ? and Month(dataVencimento) = ? and user = ? order by operacao"; 
    $sqlpreparado = $conexao->prepare($sql);
    $sqlpreparado->bind_param("ssi", $ano,$mes,$id);
    $sqlpreparado->execute();
    do{
    $sqlpreparado->bind_result(
            $conta["id"], 
            $conta["dataVencimento"], 
            $conta["descricao"],
            $conta["valor"],
            $conta["operacao"],
            $conta["situacao"],
            $conta["user"] );
    array_push($vetorListaConta, $conta); 
    $conta = null;
    }while($sqlpreparado->fetch());
    return $vetorListaConta;
}