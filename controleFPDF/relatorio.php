<?php

require_once './fpdf.php';

function gerarPDF($lsP, $mes, $ano, $nome) {
    $pdf = new FPDF("p", "pt", "A4");
    $pdf->AddPage();
    $pdf->SetFont('Arial', 'B', 15);
    $pdf->Cell(40, 20, "Relatorio de Contas a Pagar", 0, 1, "c");
    $pdf->Cell(40, 20, "Referente ao Mês $mes de $ano ", 0, 1, "c");
    //$pdf->Ln();
    $pdf->Cell(40, 20, "Usuário: $nome ", 0, 1, "c");
    $pdf->Ln();
    $pdf->SetFont('Arial', '', 10);

    $pdf->Cell(140, 30, "Data de Vencimento ", 1, 0, "c");
    $pdf->Cell(140, 30, "Descrição", 1, 0, "c");
    $pdf->Cell(85, 30, "Valor", 1, 0, "c");
    $pdf->Cell(85, 30, "Operação", 1, 0, "c");
    $pdf->Cell(85, 30, "Situação", 1, 1, "c");

    $tempTotal = 0;
    foreach ($lsP as $conta) {
        if ($conta['dataVencimento'] != "") {
            if ($conta["operacao"] == "E") {
                $tempTotal = $tempTotal + $conta["valor"];
            } else {
                $tempTotal = $tempTotal - $conta["valor"];
            }
            $data = implode('/', array_reverse(explode('-', $conta['dataVencimento'])));
            $pdf->Cell(140, 30, $data, 1, 0, "c");
            $pdf->Cell(140, 30, $conta["descricao"], 1, 0, "c");
            $pdf->Cell(85, 30, "R$".number_format($conta["valor"], 2, ',', '.'), 1, 0, "c");
            $pdf->Cell(85, 30, $conta["operacao"] == "E" ? "Entrada" : "Saida", 1, 0, "c");
            $pdf->Cell(85, 30, $conta["situacao"] == "A" ? "Aberta" : "Fechada", 1, 1, "c");
        }
       }
        $pdf->Cell(280, 30, "Total:", 1, 0, "c");
        $pdf->Cell(255, 30, "R$".number_format($tempTotal, 2, ',', '.'), 1, 1, "c");
        $pdf->Output();
    
}
