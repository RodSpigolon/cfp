<?php require_once("../controleDataBase/conecta.php"); //conecta no banco  ?> 
<?php require_once("../controleDataBase/bdConta.php"); //conecta no banco  ?> 
<?php require_once("./manipAqv.php"); //conecta no banco  ?> 
<?php require_once("../controleContas/conta.php"); //conecta no banco  ?> 
<?php require_once("../controleUser/controleUsuario.php"); //conecta no banco  ?> 
<?php require_once("./relatorio.php"); //conecta no banco  ?> 

<?php

if (isset($_GET["id"])){
    $ano = $_GET["ano"];
    $mes = $_GET["mes"];
    $id = $_GET["idUser"];
    $id=(intval($id)); 
$lsP = listarToPDF($conexao, $ano, $mes, $id);
if ($_GET["id"] == 1) {   
    exportarContas($lsP);
    setMsgOk("Contas Exportadas com Sucesso!");
    header("location: ../index.php");
} else if ($_GET["id"] == 2) {
    $nome = getNomeLogado();
    gerarPDF($lsP,$mes,$ano,$nome);
}}

if (isset($_POST["idImp"])) {
    $nome_arquivo = $_FILES["arquivo"] ["name"];
    $nome_arquivo_tmp = $_FILES["arquivo"]["tmp_name"];

    if (move_uploaded_file($nome_arquivo_tmp, "../uploads/" . $nome_arquivo)) {
        $msg = "ok";
    } else {
        $msg = "fail";
    }
    $import = importarContas($nome_arquivo);
    $array = explode(";", $import);
    //var_dump($array);

    for ($c = -1; $c < (count($array) - 10); $c = $c + 6) {
        $obj = new conta();
        $obj->dataValidade = $array[$c + 1];
        $obj->descricao = $array[$c + 2];
        $obj->valor = $array[$c + 3];
        $obj->operacao = $array[$c + 4];
        $obj->situacao = $array[$c + 5];
        $obj->id = $array[$c + 6];
        insereConta($conexao, $obj);
        
    }
    
    setMsgOk("Contas Importadas com Sucesso!");
    header("location: ../index.php");
}